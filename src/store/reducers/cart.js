import * as actionType from "../actions/actionTypes";

const initialState = {
  order: {},
  purchasing: false,
  loading: false,
  error: ''
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionType.SHOW_MODAL:
      return {...state, purchasing: true};
    case actionType.CLOSE_MODAL:
      return {...state, purchasing: false};
    case actionType.ADD_TO_CART:
      if (state.order[action.order.name]) {
        return {
          ...state, order: {
            ...state.order, [action.order.name]: {price: action.order.price, count: state.order[action.order.name].count + 1}
          },

        }
      } else {
          return {
            ...state, order: {
              ...state.order, [action.order.name]: {price: action.order.price, count: 1}
            }
          }
      }
    case actionType.REMOVE_FROM_CART:
      if (state.order[action.id].count > 0) {
        return {
          ...state, order: {
            ...state.order, [action.id]: {price: state.order[action.id].price, count: state.order[action.id].count - 1}
          }
        };
      }
      return state;
    case actionType.COFFEE_ORDER_REQUEST:
      return {...state, loading: true};
    case actionType.COFFEE_ORDER_SUCCESS:
      return {...state, loading: false};
    case actionType.COFFEE_ORDER_FAILURE:
      return {...state, error: action.error};
    default:
      return state;
  }
};

export default reducer;