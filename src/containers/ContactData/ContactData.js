import React, {Component} from 'react';
import {connect} from "react-redux";
import './ContactData.css';
import {Button} from "material-ui";
import {onPlaceOrder} from "../../store/actions/cart";
import Spinner from "../../components/Spinner/Spinner";


class ContactData extends Component {
  state = {
    name: '',
    email: '',
    address: '',
    phone: ''
  };

  valueChanged = (e) => {
    const name = e.target.name;
    this.setState({[name]: e.target.value});
  };

  orderHandler = (event) => {
    event.preventDefault();
    const order = {
      order: this.props.order,
      customer: {
        name: this.state.name,
        email: this.state.email,
        address: this.state.address,
        phone: this.state.phone
      }
    };
      this.props.onPlaceOrder(order);
  };

  render() {
    return (
      <div className="ContactData">
        <h4>Ваши контактные данные:</h4>
        {!this.props.loading ?
        <form>
          <input className="Input" type="text" name="name" placeholder="Your Name"
                 value={this.state.name} onChange={this.valueChanged}/>
          <input className="Input" type="email" name="email" placeholder="Your Mail"
                 value={this.state.email} onChange={this.valueChanged}/>
          <input className="Input" type="text" name="address" placeholder="Address"
                 value={this.state.address} onChange={this.valueChanged}/>
          <input className="Input" type="text" name="phone" placeholder="Phone"
                 value={this.state.phone} onChange={this.valueChanged}/>
          <Button variant="raised" color="primary" onClick={this.orderHandler}>Заказать</Button>
        </form> : <Spinner/>}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    loading: state.cart.loading,
    order: state.cart.order
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onPlaceOrder: (order) => dispatch(onPlaceOrder(order))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ContactData);
