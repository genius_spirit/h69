import React, {Component} from 'react';
import {connect} from "react-redux";
import './CoffeeShop.css';
import {addCoffeeToCart, getCoffeeList} from "../../store/actions/coffee";
import CoffeeCart from "../CoffeeCart/CoffeeCart";

class CoffeeShop extends Component {
  componentDidMount() {
    this.props.getCoffeeList();
  }
  render() {
    return(
      <div className="container">
        <div className="coffeeList">
          {Object.keys(this.props.coffeeList).map(itemId => {
            return (
              <div className="coffeeList__item" key={itemId}>
                <div className="coffeeList__img"><img src={this.props.coffeeList[itemId].link} alt="coffee"/></div>
                <div className="coffeeList__title">
                  <p className="coffeeList__name">{this.props.coffeeList[itemId].name}</p>
                  <p className="coffeeList__price">{this.props.coffeeList[itemId].price} kgs</p>
                </div>
                <div className="coffeeList__btn">
                  <i className="material-icons md"
                     onClick={() => this.props.addCoffeeToCart(this.props.coffeeList[itemId])}>shopping_cart</i>
                </div>
              </div>
            )
          })}
        </div>
        <div className="coffeeCart"><CoffeeCart/></div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    coffeeList: state.coffee.coffeeList
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getCoffeeList: () => dispatch(getCoffeeList()),
    addCoffeeToCart: (order) => dispatch(addCoffeeToCart(order))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(CoffeeShop);