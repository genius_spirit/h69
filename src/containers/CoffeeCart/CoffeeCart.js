import React, { Component } from 'react';
import {connect} from "react-redux";
import './CoffeeCart.css';
import {closeModal, removeCoffeeFromCart, showModal} from "../../store/actions/cart";
import {Button} from "material-ui";
import Modal from "../../components/Modal/Modal";
import ContactData from "../ContactData/ContactData";

class CoffeeCart extends Component {
  getPrice = (order) => {
    return order.price * order.count;
  };

  getTotalPrice = () => {
    return Object.keys(this.props.order).reduce((acc, key) => {
      acc += this.props.order[key].price * this.props.order[key].count;
      return acc;
    }, 150);
  };

  isDisabled = () => {
    let count = Object.keys(this.props.order).reduce((acc, key) => {
      acc += this.props.order[key].count;
      return acc;
    }, 0);
    return count === 0;
  };

  render() {
    return(
       <div className="cart">
         <div className="cart-orders">
           {Object.keys(this.props.order).map(itemId => {
             return <p key={itemId} className="cart-orders__item"
                       onClick={() => this.props.removeCoffeeFromCart(itemId)}>
               {itemId} x {this.props.order[itemId].count}
             <span className="cart-orders__price">{this.getPrice(this.props.order[itemId])} kgs</span></p>
           })}
         </div>
         <div className="cart-orders__total">
           <p>Доставка: 150 kgs</p>
           <p style={{fontWeight: '700'}}>Итого: {this.getTotalPrice()}</p>
         </div>
         <Button variant="raised" color="primary"
                 onClick={this.props.showModal}
                 disabled={this.isDisabled()}>Сделать заказ</Button>
         {this.props.purchasing ?
         <Modal show={this.props.purchasing} closed={this.props.closeModal}>
           <ContactData/>
         </Modal> : null}
       </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    order: state.cart.order,
    purchasing: state.cart.purchasing
  }
};

const mapDispatchToProps = dispatch => {
  return {
    removeCoffeeFromCart: (id) => dispatch(removeCoffeeFromCart(id)),
    showModal: () => dispatch(showModal()),
    closeModal: () => dispatch(closeModal())
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(CoffeeCart);